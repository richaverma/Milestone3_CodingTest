import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, Query } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientInterceptor } from './http-client.interceptor';
import { PlaceAppointmentModule } from './place-appointment/place-appointment.module';
import { QueryModule } from './query/query.module';
import { ViewModule } from './view/view.module';
import { WelcomeModule } from './welcome/welcome.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WelcomeModule,
    QueryModule,
    ViewModule,
    PlaceAppointmentModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
{
    provide: HTTP_INTERCEPTORS,
  
    useClass:HttpClientInterceptor,
  
    multi:true
  
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
