import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import {AppointmentService } from './health.service';

describe('AppointmentService', () => {
  let service: AppointmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers: [

      HttpClient,

      FormBuilder

    ],

    imports: [

      RouterTestingModule,

      HttpClientModule

    ]

  });
    service = TestBed.inject(AppointmentService);
    
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
