import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QueryRoutingModule } from './query-routing.module';
import { QueryComponent } from './query/query.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    QueryComponent
  ],
  imports: [
    CommonModule,
    QueryRoutingModule,
    ReactiveFormsModule,
    FormsModule, 
    HttpClientModule
  ]
})
export class QueryModule { }
