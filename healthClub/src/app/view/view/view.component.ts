import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/health.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  url ="http://localhost:3000/appointments";
  constructor(public api:AppointmentService) { }
  data:any;
  ngOnInit(): void {
    this.api.get(this.url).subscribe(res=>{
      this.data =res;
    })
  }
  trackByIndex = (index:number):number =>{
    return index;
  }

}
