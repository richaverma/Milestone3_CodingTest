import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaceappointmentComponent } from './place-appointment/placeappointment/placeappointment.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view/view/view.component';
import { WelcomeComponent } from './welcome/welcome/welcome.component';

const routes: Routes = [
  {path: '', redirectTo: 'welcome', pathMatch: 'full'},
  {path: 'welcome', component:WelcomeComponent},
  {path: 'view',component:ViewComponent},
  {path: 'query',component:QueryComponent},
  {path: 'placeappointment', component:PlaceappointmentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
