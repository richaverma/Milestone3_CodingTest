import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaceAppointmentRoutingModule } from './place-appointment-routing.module';
import { PlaceappointmentComponent } from './placeappointment/placeappointment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PlaceappointmentComponent
  ],
  imports: [
    CommonModule,
    PlaceAppointmentRoutingModule,
    FormsModule,    //import here
    ReactiveFormsModule

  ]
})
export class PlaceAppointmentModule { }
