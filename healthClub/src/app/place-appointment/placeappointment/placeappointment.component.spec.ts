import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { PlaceappointmentComponent } from './placeappointment.component';

describe('PlaceappointmentComponent', () => {
  let component: PlaceappointmentComponent;
  let fixture: ComponentFixture<PlaceappointmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaceappointmentComponent ],
      providers: [

        HttpClient,

        FormBuilder

      ],

      imports: [

        RouterTestingModule,

        HttpClientModule

      ]

    
      
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaceappointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
